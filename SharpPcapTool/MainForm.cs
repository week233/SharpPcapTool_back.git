﻿using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SharpPcapTool
{
    public partial class MainForm : Form
    {
        private LibPcapLiveDeviceList deviceList; // 本机设备列表

        private ArpTool arpTool = null;

        private List<Tuple<IPAddress, PhysicalAddress>> IPMACMapList;

        public MainForm()
        {
            InitializeComponent();
            IPMACMapList = new List<Tuple<IPAddress, PhysicalAddress>>();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            deviceList = LibPcapLiveDeviceList.Instance;

            if (deviceList.Count < 1)
            {
                throw new Exception("没有发现本机上的网络设备");
            }

            cmbDeviceList.DataSource = deviceList;
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            if (button.Text.Equals("扫描"))
            {
                IPAddress startIP, endIP;
                if (!IPAddress.TryParse(txbStartIP.Text, out startIP) || !IPAddress.TryParse(txbEndIP.Text, out endIP))
                {
                    MessageBox.Show("不合法的IP地址");
                    return;
                }

                IP start = new IP(startIP);
                IP end = new IP(endIP);
                if (end.SmallerThan(start))
                {
                    MessageBox.Show("起始地址大于结束地址");
                    return;
                }

                button.Text = "停止";
                IPMACMapList.Clear();
                lsbIPMap.Items.Clear();
                btnArp.Enabled = btnArpGetway.Enabled = false;
                arpTool.ScanLAN(start, end);
            }
            else
            {
                arpTool.StopScanLan();
                button.Text = "扫描";
            }
        }

        void arpTool_ResolvedEvent(object sender, ResolvedEventArgs e)
        {
            IPMACMapList.Add(new Tuple<IPAddress, PhysicalAddress>(e.IPAddress, e.PhysicalAddress));
            this.Invoke(new Action(() =>
            {
                lsbIPMap.Items.Add(string.Format("{0} -> {1}", e.IPAddress, e.PhysicalAddress));
            }));
        }

        private void cmbDeviceList_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 获取相应的设备网关MAC，IP，本机MAC，IP
            var device = deviceList[(sender as ComboBox).SelectedIndex];
            arpTool = new ArpTool(device);
            arpTool.ScanStopedEvent += arpTool_ScanStopedEvent;
            arpTool.ResolvedEvent += arpTool_ResolvedEvent;
            txbGetwayIP.Text = arpTool.GetwayIP.ToString();
            txbGetwayMAC.Text = arpTool.GetwayMAC.ToString();
            txbLocalIP.Text = arpTool.LocalIP.ToString();
            txbLocalMAC.Text = arpTool.LocalMAC.ToString();
            txbStartIP.Text = txbEndIP.Text = arpTool.GetwayIP.ToString();

        }

        void arpTool_ScanStopedEvent(object sender, EventArgs e)
        {
            this.Invoke(new Action(() => { btnScan.Text = "扫描"; }));
            MessageBox.Show("扫描结束");
        }

        private void lsbIPMap_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender as ListBox).SelectedIndex >= 0)
            {
                btnArp.Enabled = true;
                btnArpGetway.Enabled = true;
            }
        }

        /// <summary>
        /// 主机欺骗
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnArp_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            if (button.Text.Equals("ARP主机欺骗"))
            {
                var destIP = IPMACMapList[lsbIPMap.SelectedIndex].Item1;
                var destMAC = IPMACMapList[lsbIPMap.SelectedIndex].Item2;
                arpTool.ARPSpoofing2(destIP, destMAC);
                button.Text = "停止";
            }
            else
            {
                arpTool.StopARPSpoofing();
                button.Text = "ARP主机欺骗";
            }
        }

        /// <summary>
        /// 网关欺骗
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnArpGetway_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            if (button.Text.Equals("ARP网关欺骗"))
            {
                var destIP = IPMACMapList[lsbIPMap.SelectedIndex].Item1;
                var destMAC = IPMACMapList[lsbIPMap.SelectedIndex].Item2;
                arpTool.ARPSpoofing1(destIP);
                button.Text = "停止";
            }
            else
            {
                arpTool.StopARPSpoofing();
                button.Text = "ARP网关欺骗";
            }
        }

        private void btnArpStorm_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            if (button.Text.Equals("ARP风暴"))
            {
                // 准备IP段
                List<IPAddress> ipList = new List<IPAddress>();
                foreach (var tuple in IPMACMapList)
                {
                    ipList.Add(tuple.Item1);
                }
                button.Text = "停止";
                arpTool.ARPStorm(ipList);
            }
            else
            {
                arpTool.StopARPSpoofing();
                button.Text = "ARP风暴";
            }
        }

        private void menuRead_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "文本文件(*.txt)|*.txt";
            openFileDialog.Multiselect = false;
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(openFileDialog.FileName))
                {
                    string line = null;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] IPMAC = line.Split('|');
                        if (IPMAC[0] != null && IPMAC[1] != null)
                        {
                            IPAddress ip = IPAddress.Parse(IPMAC[0]);
                            PhysicalAddress mac = PhysicalAddress.Parse(IPMAC[1]);
                            IPMACMapList.Add(new Tuple<IPAddress, PhysicalAddress>(ip, mac));
                            lsbIPMap.Items.Add(string.Format("{0} -> {1}", ip, mac));
                        }
                    }
                }
            }
        }

        private void menuSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "文本文件(*.txt)|*.txt";
            saveFileDialog.FileName = "arp.txt";
            if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(saveFileDialog.FileName))
                {
                    foreach (var IPMAC in IPMACMapList)
                    {
                        string line = string.Format("{0}|{1}", IPMAC.Item1, IPMAC.Item2);
                        writer.WriteLine(line);
                    }
                }
            }
        }
    }
}
